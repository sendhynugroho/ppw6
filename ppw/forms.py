from django import forms
from . import models

class BuatPesan(forms.ModelForm):
    pesan = forms.CharField(label='', widget=forms.Textarea
                            (attrs={'placeholder':'Write your message here'}))
    class Meta:
        model = models.Pesan
        fields = ['pesan']