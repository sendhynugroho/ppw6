from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpResponse
from django.http import HttpRequest
from .views import message
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException 
from ppw import views

# Create your tests here.
class PPWPageTest(TestCase) :
        
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'ppw/registration.html')

    def test_ppw_using_index_func(self):
        response_func = resolve('/')
        self.assertEqual(response_func.func, views.login)

    def test_fill_message(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['pesan'] = 'test'
        message(request)

    # check views.py function return json data of books
    def test_story_9_comic_json(self):
        response_func = resolve('/comic/')
        self.assertEqual(response_func.func,views.data_comic)

    def test_story_9_movie_json(self):
        response_func = resolve('/movie/')
        self.assertEqual(response_func.func,views.data_movie)

    def test_story_9_quilting_json(self):
        response_func = resolve('/quilting/')
        self.assertEqual(response_func.func,views.data_quilting)


class Story7FunctionalTest(TestCase) :

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
        super(Story7FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/message')
        message = selenium.find_element_by_id('id_pesan')
        submit = selenium.find_element_by_id('submit')
        message.send_keys('Coba Coba')
        submit.send_keys(Keys.RETURN)
        self.assertIn("Coba Coba", self.selenium.page_source)

    def test_about_text_in_homepage(self):
        selenium = self.selenium
        selenium.get('http://ppw-f-story5.herokuapp.com/')
        title = selenium.find_element_by_tag_name('h1')
        self.assertIn("About Me", self.selenium.page_source)

    def test_submit_button_on_form_page(self):
        selenium = self.selenium
        selenium.get('http://ppw-f-story5.herokuapp.com/form/')
        try:
            selenium.find_element_by_id('contact-submit')
            return True
        except NoSuchElementException:
            return False
        
    def test_margin_on_homepage_body(self):
        selenium = self.selenium
        selenium.get('http://ppw-f-story5.herokuapp.com/')
        body = selenium.find_element_by_tag_name('body')
        body.value_of_css_property("margin:0px")

    def test_homepage_background_color(self):
        selenium = self.selenium
        selenium.get('http://ppw-f-story5.herokuapp.com/')
        body = selenium.find_element_by_tag_name('body')
        body.value_of_css_property("background-color:#051727")



    

