from django.db import models
from django.utils import timezone

# Create your models here.
class Pesan(models.Model):
    pesan = models.TextField(max_length=300)
    tanggal = models.DateTimeField(default=timezone.now)
