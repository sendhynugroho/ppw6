from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect, HttpResponse, HttpRequest, HttpResponseBadRequest
from django.core import serializers
import requests
import json
from . import forms
from . import models
from .models import Pesan
from django.contrib.auth import logout

# Create your views here.
def profile(request):
    return render(request, 'ppw/profile.html')

def message(request):
    status = Pesan.objects.all().order_by('-tanggal')
    if request.method == 'POST':
        form = forms.BuatPesan(request.POST)
        if form.is_valid():
            form.save()
            return redirect('message')
    else:
        form = forms.BuatPesan()
    return render(request, 'ppw/index.html', {'form' : form, 'event': status})

def delete(request):
        event = Pesan.objects.all().delete()
        return redirect('message')

def library(request):
    return render(request, 'ppw/library.html')

def data_quilting(request):
    get_json_obj = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
    result = (get_json_obj.json())['items']
    return_data = {"data": result }
    json_data = json.dumps(return_data)
    return HttpResponse(json_data, content_type="application/json")

def data_movie(request):
    get_json_obj = requests.get('https://www.googleapis.com/books/v1/volumes?q=movie')
    result = (get_json_obj.json())['items']
    return_data = {"data": result }
    json_data = json.dumps(return_data)
    return HttpResponse(json_data, content_type="application/json")

def data_comic(request):
    get_json_obj = requests.get('https://www.googleapis.com/books/v1/volumes?q=comic')
    result = (get_json_obj.json())['items']
    return_data = {"data": result }
    json_data = json.dumps(return_data)
    return HttpResponse(json_data, content_type="application/json")

def home(request):
	return render(request, 'ppw/home.html')

def login(request):
	return render(request, 'ppw/registration.html')

def logout_page(request):
    if (request.user.is_authenticated):
        logout(request)
    
    return render(request, 'ppw/registration.html')





