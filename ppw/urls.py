from django.urls import path
from django.conf.urls import *
from . import views
from .views import message
from .views import profile
from .views import library
from .views import data_quilting
from .views import data_movie
from .views import data_comic

#url for app
urlpatterns = [
    path('', views.login, name ='login'),
    path('logout/', views.logout_page, name ='logout_page'),
    path('message/', views.message, name='message'),
    path('profile/', views.profile, name='profile'),
    path('delete/', views.delete, name='delete'),
    path('library/', views.library, name='library'),
    path('quilting/',views.data_quilting,name="data_quilting"),
    path('movie/',views.data_movie, name="data_movie"),
    path('comic/',views.data_comic, name="data_comic"),
    url(r'^auth/', include('social_django.urls', namespace='social')),
]
