$(document).ready(function () {
    $("#style1").click(function () {
        $("body").css('background-color', 'whitesmoke');
        $("h1").css('color', 'black');
        $("td").css('color', 'black');
        $("p").css('color', 'black');
        $("li").css('color', 'black');
    });
    
    $("#style2").click(function () {
        $("body").css('background-color', '#191919');
        $("h1").css('color', 'white');
        $("td").css('color', 'white');
        $("p").css('color', 'white');
        $("li").css('color', 'white');
    });
});
